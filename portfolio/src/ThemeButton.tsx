import React from 'react'
import {
    IconButton,
    useColorMode,
    useColorModeValue,
    Link
} from '@chakra-ui/react'
import { SunIcon, MoonIcon } from '@chakra-ui/icons'

const ThemeButton = () => {
    const { toggleColorMode } = useColorMode()

    return (
        <IconButton
            aria-label="Toggle theme"
            // color={useColorModeValue('white', 'grey')}
            // bg={useColorModeValue('primary', 'secondary')}
            colorScheme={useColorModeValue('teal', 'orange')}
            icon={useColorModeValue(<MoonIcon />, <SunIcon />)}
            onClick={toggleColorMode}
        ></IconButton>
    )
}

export default ThemeButton
