import React from 'react'
import styled from '@emotion/styled'
import { Heading, Text, useColorModeValue, useTheme } from '@chakra-ui/react'

interface TimelineProp {
    timelineColor: string
}

const TimeLine = styled('div')`
    position: relative;
    margin: 0 auto;

    &:after {
        content: '';
        position: absolute;
        width: 6px;
        background-color: ${(props: TimelineProp) => props.timelineColor};
        top: 0;
        bottom: 0;
        left: 50%;
        margin-left: -3px;
    }
`
interface BioBoxProp {
    pos: 'left' | 'right'
    roundColor: string
}

const BioBox = styled('div')`
    padding: 20px 30px;
    position: relative;
    width: 50%;
    display: flex;
    flex-direction: column;
    left: ${(props: BioBoxProp) => (props.pos === 'left' ? '0' : '50%')};
    align-items: ${(props: BioBoxProp) =>
        props.pos === 'left' ? 'end' : 'start'};
    text-align: ${(props: BioBoxProp) =>
        props.pos === 'left' ? 'end' : 'start'};

    &:after {
        content: '';
        position: absolute;
        width: 25px;
        height: 25px;
        right: -13px;
        background-color: white;
        border: 4px solid ${(props: BioBoxProp) => props.roundColor};
        top: 15px;
        border-radius: 50%;
        z-index: 1;
        ${(props: BioBoxProp) => (props.pos === 'right' ? 'left: -12px;' : '')}
    }

    &:hover:after {
        background-color: ${(props: BioBoxProp) => props.roundColor};
        cursor: pointer;
    }
`
interface BioElement {
    id: number
    date: number
    company: string
    title: string
}

interface BioSectionProp {
    bio: Array<BioElement>
}

const BioSection = (props: BioSectionProp) => {
    const theme = useTheme()
    const timelineColor = useColorModeValue('gray', 'white')
    const roundColor = useColorModeValue(
        theme.colors.primary,
        theme.colors.secondary
    )

    return (
        <TimeLine timelineColor={timelineColor}>
            {props.bio.map((element, index) => {
                return (
                    <BioBox
                        pos={index % 2 === 0 ? 'left' : 'right'}
                        roundColor={roundColor}
                        key={index}
                    >
                        <Heading as="h4" variant="bio-date">
                            {element.date}
                        </Heading>
                        <Heading as="h4" variant="bio-company">
                            {element.company}
                        </Heading>
                        <Text>{element.title}</Text>
                    </BioBox>
                )
            })}
        </TimeLine>
    )
}

export default BioSection
