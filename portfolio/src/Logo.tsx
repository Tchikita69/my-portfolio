import Link from 'next/link'
import Image from 'next/image'
import { Text, useColorModeValue } from '@chakra-ui/react'
import styled from '@emotion/styled'
import React from 'react'

const LogoBox = styled.span`
    font-weight: bold;
    font-size: 20px;
    display: inline-flex;
    align-items: center;
    height: 30px;
    line-height: 20px;
    padding: 20px;
`

const Logo = () => {
    const logoImg = `/images/Logo${useColorModeValue('Light', 'Dark')}.png`

    return (
        <Link href="/">
            <a>
                <LogoBox>
                    <Image
                        src={logoImg}
                        width={42}
                        height={24}
                        alt="logo"
                    ></Image>
                    <Text
                        color={useColorModeValue('gray.800', 'whiteAlpha.900')}
                        fontWeight="bold"
                        ml={3}
                    >
                        Grégory Grondin
                    </Text>
                </LogoBox>
            </a>
        </Link>
    )
}

export default Logo
