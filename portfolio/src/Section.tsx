
import { motion } from 'framer-motion'
import { Box, type BoxProps } from '@chakra-ui/react'
import React from 'react'

export const MotionBox = motion<BoxProps>(Box)

interface SectionPropsType  {
    children?: React.ReactChild | React.ReactChild[]
}

const Section = ({ children } : SectionPropsType) => (
    <MotionBox
    initial={{ y: 10, opacity: 0 }}
    animate={{ y: 0, opacity: 1, transition: {duration: 0.5}}}
    mb={6}>
        {children}
    </MotionBox>
)

export default Section