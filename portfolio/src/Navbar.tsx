import React from 'react'
import Logo from './Logo'
import { default as NextLink } from 'next/link'
import {
    Container,
    Box,
    Link,
    Stack,
    Heading,
    Flex,
    Menu,
    MenuItem,
    MenuList,
    MenuButton,
    IconButton,
    useColorModeValue
} from '@chakra-ui/react'
import { HamburgerIcon } from '@chakra-ui/icons'
import { MenuItems, MobileMenuItems, MenuItemProp } from './MenuItems'
import ThemeButton from './ThemeButton'

interface LinkItemProps {
    href: string
    currentPath: string
    children: string
}

const LinkItem = ({ href, currentPath, children }: LinkItemProps) => {
    const active = true
        ? currentPath.toUpperCase() === href.toUpperCase()
        : false
    const inactiveColor = useColorModeValue('gray200', 'whiteAlpha.900')

    return (
        <NextLink href={href} passHref>
            <Link
                p={2}
                bg={active ? 'primary' : undefined}
                color={active ? 'white' : inactiveColor}
            >
                {children}
            </Link>
        </NextLink>
    )
}

interface NavbarProps {
    path: string
}

const Navbar = (props: NavbarProps) => {
    const { path } = props
    const array = [1, 2, 3]

    return (
        <Box
            position="fixed"
            as="nav"
            w="100%"
            bg={useColorModeValue('#ffffff40', '#20202380')}
            style={{ backdropFilter: 'blur(10px' }}
            zIndex={1}
            {...props}
        >
            <Container
                display="flex"
                p={2}
                maxW="container.md"
                wrap="wrap"
                align="center"
                justify="space-between"
            >
                <Flex align="center" mr={5}>
                    <Heading as="h1" size="lg" letterSpacing={'tighter'}>
                        <Logo />
                    </Heading>
                </Flex>

                <Stack
                    direction={{ base: 'column', md: 'row' }}
                    display={{ base: 'none', md: 'flex' }}
                    width={{ base: 'full', md: 'auto' }}
                    alignItems="center"
                    flexGrow={1}
                >
                    {MenuItems.map((item: MenuItemProp, index: number) => {
                        return (
                            <LinkItem
                                key={index}
                                href={item.href}
                                currentPath={path}
                            >
                                {item.name}
                            </LinkItem>
                        )
                    })}
                </Stack>
                <ThemeButton />
                <Box flex={1} textAlign="right">
                    <Box ml={2} display={{ base: 'inline-block', md: 'none' }}>
                        <Menu isLazy id="navbar-menu">
                            <MenuButton
                                as={IconButton}
                                icon={<HamburgerIcon />}
                                variant="outline"
                                aria-label="Options"
                            />
                            <MenuList>
                                {MobileMenuItems.map(
                                    (item: MenuItemProp, index: number) => {
                                        return (
                                            <NextLink
                                                key={index}
                                                href={item.href}
                                                passHref
                                            >
                                                <MenuItem as={Link}>
                                                    {item.name}
                                                </MenuItem>
                                            </NextLink>
                                        )
                                    }
                                )}
                            </MenuList>
                        </Menu>
                    </Box>
                </Box>
            </Container>
        </Box>
    )
}

export default Navbar
