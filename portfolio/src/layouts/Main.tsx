import React, { Suspense } from 'react'
import Head from 'next/head'
import { Box, Container } from '@chakra-ui/react'
import Navbar from '../Navbar'
import Footer from './Footer'
import { Canvas } from '@react-three/fiber'
import { OrbitControls } from '@react-three/drei'
import Avatar from '../../public/Avatar'

const Main = ({ children, router }) => {
    return (
        <Box as="main" pb={8}>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                ></meta>
                <title>Grégory Grondin - Portfolio</title>
            </Head>
            <Navbar path={router.asPath} />
            <Container maxW="container.md" pt={20}>
                <Canvas
                    shadows
                    style={{
                        height: '42vh',
                        display: 'flex',
                        justifyContent: 'center'
                    }}
                    camera={{ position: [0, 0, 20], fov: 70, zoom: 20 }}
                >
                    <ambientLight intensity={1.25} />
                    <ambientLight intensity={0.1} />
                    <directionalLight intensity={0.4} />
                    <Suspense fallback={null}>
                        <Avatar position={[0, -1.0, 0]} />
                    </Suspense>
                    <OrbitControls />
                </Canvas>
                {children}
            </Container>
            <Footer />
        </Box>
    )
}

export default Main
