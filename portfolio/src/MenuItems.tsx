export interface MenuItemProp {
    name: string
    href: string
}

export const MenuItems = [
    {
        name: 'Works',
        href: '/works'
    },
    {
        name: 'Posts',
        href: '/posts'
    }
]

export const MobileMenuItems = [
    {
        name: 'Works',
        href: '/works'
    },
    {
        name: 'Posts',
        href: '/posts'
    }
]
