/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
*/

import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'

export default function Model({ ...props }) {
  const group = useRef()
  const { nodes, materials } = useGLTF('/my_desktop.glb')
  return (
    <group ref={group} {...props} dispose={null}>
      <mesh geometry={nodes.Cube.geometry} material={materials.BROWN} />
      <group position={[0, 2, 0]}>
        <mesh geometry={nodes.Cube001_1.geometry} material={materials['Material.001']} />
        <mesh geometry={nodes.Cube001_2.geometry} material={materials.Brown} />
        <mesh geometry={nodes.Cube001_3.geometry} material={materials.BROWN} />
      </group>
      <mesh geometry={nodes.Cube002.geometry} material={materials.BROWN} position={[0, 0, -4.07]} />
      <mesh geometry={nodes.Cube003.geometry} material={materials.BROWN} position={[-1.86, 0, -4.07]} />
      <mesh geometry={nodes.Cube004.geometry} material={materials.BROWN} position={[0, 1.01, 0]} />
      <group position={[0, 1.24, 0]}>
        <mesh geometry={nodes.Cube005_1.geometry} material={materials.Material} />
        <mesh geometry={nodes.Cube005_2.geometry} material={materials.BROWN} />
      </group>
    </group>
  )
}

useGLTF.preload('/my_desktop.glb')
