import { extendTheme } from '@chakra-ui/react'
import { mode, StyleFunctionProps } from '@chakra-ui/theme-tools'
import { Dict } from '@chakra-ui/utils'

const colors = {
    bglgt: '#f8edeb',
    bgdrk: '#14213d',
    primary: '#0a9396',
    secondary: '#fca311',
    tiredGrey: '#e5e5e5'
}

const styles = {
    global: (props: Dict<unknown> | StyleFunctionProps) => ({
        body: {
            bg: mode('bglgt', 'bgdrk')(props)
        }
    })
}

const components = {
    Heading: {
        variants: {
            'section-title': (props: Dict<unknown> | StyleFunctionProps) => ({
                textDecoration: 'underline',
                fontSize: 20,
                textUnderlineOffset: 10,
                textDecorationColor: mode('primary', 'secondary')(props),
                textDecorationThickness: 7,
                marginTop: 5,
                marginBottom: 30
            }),
            'bio-date': () => ({
                fontSize: 18,
                marginBottom: 1
            }),
            'bio-company': () => ({
                fontSize: 16,
                marginBottom: 1
            })
        }
    },
    Text: {
        variants: {
            paragraph: {
                textAlign: 'justify',
                textIndent: '1.5em'
            }
        }
    },
    Link: {
        variants: {
            externalLink: (props: Dict<unknown> | StyleFunctionProps) => ({
                color: mode('primary', 'secondary')(props)
            }),
            usefulLinks: {
                textDecoration: 'none',
                display: 'flex',
                gap: 2,
                fontSize: 18
            }
        }
    }
}

const fonts = {}

const config = {
    initialColorMode: 'dark',
    useSystemColorMode: true
}

const theme = extendTheme({ config, styles, components, fonts, colors })
export default theme
