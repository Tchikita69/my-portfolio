import type { AppProps } from 'next/app'
import { ChakraProvider } from '@chakra-ui/react'
import Layout from '../src/layouts/Main'
import theme from '../theming/themes'

function MyApp({ Component, pageProps, router }: AppProps) {
    return (
        <ChakraProvider theme={theme}>
            <Layout router={router}>
                <Component {...pageProps} key={router.route}></Component>
            </Layout>
        </ChakraProvider>
    )
}

export default MyApp
