import type { NextPage } from 'next'
import {
    Box,
    Container,
    Heading,
    Text,
    Flex,
    useColorModeValue,
    Link,
    Icon,
    Button,
    List,
    ListItem,
    Image
} from '@chakra-ui/react'
import {
    AiFillGithub,
    AiFillGitlab,
    AiFillLinkedin,
    AiFillYoutube,
    AiFillInstagram
} from 'react-icons/ai'
import Section from '../src/Section'
import BioSection from '../src/BioSection'

const welcomeText =
    "Hello ! I'm Grégory, a web developer based in Lyon (France)."
const aboutMe =
    "I'm a young developer and student at Epitech Technology who like coding. Being pretty curious, I really like to learn and to put my skills to use in various fields. I also appreciate to teach and share my knowledge to people."

const Bio = [
    {
        id: 0,
        date: 2021,
        company: 'Parkours',
        title: "Coordinateur d'établissement"
    },
    {
        id: 1,
        date: 2019,
        company: 'Indépendant',
        title: 'Professeur de guitare'
    },
    {
        id: 2,
        date: 2018,
        company: 'Anset Assurances',
        title: 'Développeur web / Assistance à distance'
    },
    {
        id: 3,
        date: 2017,
        company: 'EPITECH Technology',
        title: "Obtention du titre 'Bachelor'"
    },
    {
        id: 4,
        date: 2016,
        company: 'Lycée Leconte de Lisle',
        title: 'Obtention du baccalauréat scientifique'
    }
]

const interests =
    ', Sport (Swimming, Longboard), Audiovisual creations, Design, Ballads, Reading.'

const usefulLinks = [
    {
        id: 0,
        title: 'LinkedIn',
        url: 'https://www.linkedin.com/in/grégory-grondin-155216153',
        icon: AiFillLinkedin
    },
    {
        id: 1,
        title: 'Youtube Channel',
        url: 'https://www.youtube.com/channel/UCDBETF15_7JRIVluw3c9qtA',
        icon: AiFillYoutube
    },
    {
        id: 2,
        title: 'Instagram',
        url: 'https://www.instagram.com/roregyg/?hl=fr',
        icon: AiFillInstagram
    },
    {
        id: 3,
        title: 'Github',
        url: 'https://github.com/GregoryGrondin',
        icon: AiFillGithub
    },
    {
        id: 4,
        title: 'Gitlab',
        url: 'https://gitlab.com/Tchikita69',
        icon: AiFillGitlab
    }
]
const Home: NextPage = () => {
    const profileImg = `/images/justme${useColorModeValue('light', 'dark')}.jpg`

    return (
        <Container>
            <Box
                borderRadius={'xl'}
                bg={useColorModeValue('whiteAlpha.500', 'whiteAlpha.200')}
                p={5}
                mt={10}
                mb={5}
                textAlign={'center'}
            >
                {welcomeText}
            </Box>
            <Flex align="center">
                <Box flexGrow={1}>
                    <Heading as="h2" variant="page-title">
                        Grégory Grondin
                    </Heading>
                    <p>Web developper</p>
                    <p>5th year student at EPITECH Technology</p>
                </Box>
                <Image
                    borderRadius="full"
                    boxSize="100px"
                    fit="cover"
                    src={profileImg}
                    alt="profile_picture"
                />
            </Flex>
            <Section style={{ marginBottom: 10 }}>
                <Heading as="h3" variant={'section-title'}>
                    About me
                </Heading>
                <Text variant="paragraph">{aboutMe}</Text>
            </Section>
            <Section>
                <Heading as="h3" variant={'section-title'}>
                    Experience / Education
                </Heading>
                <BioSection bio={Bio} />
                {/* <Text variant="paragraph">{aboutMe}</Text> */}
            </Section>
            <Section>
                <Heading as="h3" variant={'section-title'}>
                    Interests
                </Heading>
                <Text variant="paragraph">
                    <Link
                        href="https://www.youtube.com/channel/UCp93_W2CMdIKjVK5OITh3EQ"
                        isExternal
                        variant="externalLink"
                    >
                        Music (Composition, Guitar, Singing...)
                    </Link>
                    {interests}
                </Text>
                {/* <Text variant="paragraph">{aboutMe}</Text> */}
            </Section>
            <Section>
                <Heading as="h3" variant={'section-title'}>
                    Useful Links
                </Heading>
                <List>
                    {usefulLinks.map((element, index) => {
                        return (
                            <ListItem key={index}>
                                <Link href={element.url} isExternal>
                                    <Button
                                        variant="ghost"
                                        leftIcon={<Icon as={element.icon} />}
                                    >
                                        {element.title}
                                    </Button>
                                </Link>
                            </ListItem>
                        )
                    })}
                </List>
                {/* <Flex direction="column" gap={3}>
                    {usefulLinks.map((element, index) => {
                        return (
                            <Link
                                key={index}
                                href={element.url}
                                variant="usefulLinks"
                                isExternal
                            >
                                <Icon as={element.icon} />
                                <Text>{element.title}</Text>
                            </Link>
                        )
                    })}
                </Flex> */}
            </Section>
        </Container>
    )
}

export default Home
